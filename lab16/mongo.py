"""
Tanner Christensen
MongoDB
"""

import pymongo
from pymongo import MongoClient
import json

### Problems ###

def prob1():
    mc = MongoClient()
    db = mc.db1
    rest = db.collection1
    rest.drop()

    with open("new_restaurants.json") as f:
        lines = f.readlines()

    json_objects = [json.loads(l) for l in lines]
    rest.insert_many(json_objects)

def prob2():
    mc = MongoClient()
    db = mc.db1
    rest = db.collection1

    with open("mylans_bistro.json") as f:
        lines = f.readlines()

    json_object = json.loads(lines[0])
    rest.insert_one(json_object)

    for r in rest.find({'closing_time':"1800"}):
        print r


def prob3():
    mc = MongoClient()
    db = mc.db1
    rest = db.collection1 

    part1 = rest.find({'borough':'Manhattan'}).count()
    part2 = rest.find({'grades.grade':'A'}).count()

    part3 = [i['name'] for i in list(rest.find({'address.coord.1':{"$gt":45}}))]

    part4 = [n['name'] for n in rest.find({"name": {"$regex": ".*[G|g]rill.*"}}, {"name":1, "_id":0})]
    return part1, part2, part3, part4

def prob4():
    mc = MongoClient()
    db = mc.db1
    rest = db.collection1

    all_grills = prob3()[3]
    for n in all_grills:
        rest.update_one({'name':n}, {'$set':{'name':n.replace('Grill', 'Magical Fire Table').replace('grill', 'Magical Fire Table')}})
    
    

    for n in rest.find({}):
        rest.update_one(n, {'$set':{'restaurant_id':str(int(n['restaurant_id'])+1000)}})

    rest.remove({'grades.grade':'C'})

if __name__ == "__main__":
    prob4()
