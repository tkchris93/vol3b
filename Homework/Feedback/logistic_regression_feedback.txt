01/20/17 20:00

Basic Logistic Regression (6 points):
Score += 6

Regularized Logistic Regression (6 points):
Score += 6

Compare Basic to Regularized (3 points):
Score += 3

Multinomial Logistic Regression (6 points):
Score += 6

Compare to Sklearn (3 points):
Score += 3

Code Quality (6 points):
Score += 6

Total score: 30/30 = 100.0%

Excellent!

--------------------------------------------------------------------------------

